from setuptools import setup

setup( 
    name='kiwi-tcms', 
    version='0.1', 
    description='Measure Iperf3 bandwidth', 
    author='Ashok Vengala', 
    author_email='u1419414@utah.edu', 
    packages=['dagster_iperf3_tcms']
) 

